# This script supports integration testing of local git installs.
#
# Lists the contents of all git repositories in any directory below that specified

DIR=$1
CT=""

START_DIR=$PWD

for DIR in $(find $DIR -name '.git' | sort);
do
    echo "Directory: $DIR"
    cd $START_DIR/$DIR/..
    git ls-files -t
    echo ""
done

cd $START_DIR
