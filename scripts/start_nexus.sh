#!/usr/bin/env bash

set -uo pipefail

# Script to start a Nexus instance for testing Droppr

### Working variables, with defaults

_REQ_PW="${1:-admin123}"
_NEXUS_IP="${NEXUS_IP:-127.0.0.1}"
_NEXUS_WEB_PORT="${NEXUS_WEB_PORT:-8081}"
_NEXUS_DOCKER_PORT="${NEXUS_DOCKER_PORT:-5000}"
_NEXUS_IMAGE="${NEXUS_IMAGE:-sonatype/nexus3:3.41.1}"

_NEXUS_API_SOCKET="${_NEXUS_IP}:${_NEXUS_WEB_PORT}"

### Start docker nexus container
docker run --detach --name mynexus -p "$_NEXUS_WEB_PORT:$_NEXUS_WEB_PORT" -p "$_NEXUS_DOCKER_PORT:$_NEXUS_DOCKER_PORT" "$_NEXUS_IMAGE"

echo NEXUS_IP: "$_NEXUS_IP"
if [[ ! "$no_proxy" == *"$_NEXUS_IP"* ]]; then
	export no_proxy=$no_proxy,$_NEXUS_IP
	export NO_PROXY=$no_proxy
fi

### Wait up to five minutes for Nexus to come up.  Usually takes about 2

counter=0
while :
do
	counter=$((counter+1))

	rc=$(curl -sL -o /dev/null -w "%{http_code}" "http://$_NEXUS_API_SOCKET")
	echo Waiting for Nexus to start, attempt "$counter", curl rc = "$rc"
	if [[ $rc == 200 ]]; then
		break
	fi
	if (( counter > 30 )); then
		echo ERROR: Waited too long for Nexus to start
		set +u

		# This bit is to exit gracefully whether the script is executed or sourced
		return "$counter" 2> /dev/null; exit "$counter"
	fi

	sleep 10
done

### Reset the random initial admin password

_NEXUS_PW=$(docker exec mynexus cat /nexus-data/admin.password)
echo Random initial password: "$_NEXUS_PW"

echo Updating Nexus Admin Password
curl -X PUT "http://$_NEXUS_API_SOCKET/service/rest/v1/security/users/admin/change-password" -H  "accept: application/json" -H  "Content-Type: text/plain" -d "$_REQ_PW" -u "admin:$_NEXUS_PW"
_NEXUS_PW=$_REQ_PW

### Enable anonymous access

echo Enabling anonymous access
curl -sX PUT "http://$_NEXUS_API_SOCKET/service/rest/beta/security/anonymous" -H "accept: application/json" -H  "Content-Type: application/json" -d "{  \"enabled\": true,  \"userId\": \"anonymous\",  \"realmName\": \"NexusAuthorizingRealm\"}" -u "admin:$_NEXUS_PW" -o /dev/null

### If running on Rocky Linux, and firewallD is running, open ports

os="$(grep -i '^ID=' /etc/os-release)"
if [[ $os == *"rocky"* ]]; then
    sudo firewall-cmd --state -q
    rc=$?
    if [[ $rc == 0 ]]; then
        echo Opening rocky firewall ports
        sudo firewall-cmd --zone=public --add-port="$_NEXUS_WEB_PORT/tcp"
        sudo firewall-cmd --zone=public --add-port="$_NEXUS_DOCKER_PORT/tcp"
    fi
fi

set +u
