variables:
  GO_IMAGE: golang:1.19.5-bullseye

.go-tidy:
  script:
    - go mod tidy
    - go mod download

gobuild:
  image: $GO_IMAGE
  needs:
    - job: semantic-release:dry-run
      artifacts: true
  script:
    - !reference [".go-tidy", "script"]
    - mkdir -p bin
    - echo Running go build -o bin/droppr -ldflags "-X 'gitlab.com/hoppr/droppr/cmd.Version=${RELEASE_VERSION}' gitlab.com/hoppr/droppr/cmd.BuildTime=$(date)'"
    - go build -o bin/droppr -ldflags "-X 'gitlab.com/hoppr/droppr/cmd.Version=${RELEASE_VERSION}' -X 'gitlab.com/hoppr/droppr/cmd.BuildTime=$(date)'"
  artifacts:
    paths:
      - bin
      - go.mod
      - go.sum

gobuild:unit-test:
  image: $GO_IMAGE
  variables:
    TESTCOVERAGE_THRESHOLD: 75

  script:
    - !reference [".go-tidy", "script"]
    - echo Running go test -v ./... -coverprofile=coverage.out
    - go test -v ./... -coverprofile=coverage.out -covermode count
    - go tool cover -func=coverage.out
    - go tool cover -html=coverage.out -o coverage.html
    - go get github.com/boumenot/gocover-cobertura
    - go run github.com/boumenot/gocover-cobertura < coverage.out > coverage.xml
    - 'echo "Quality Gate: checking test coverage is above threshold ..."'
    - 'echo "Threshold             : $TESTCOVERAGE_THRESHOLD %"'
    - totalCoverage=`go tool cover -func=coverage.out | grep total | grep -Eo '[0-9]+\.[0-9]+'`
    - 'echo "Current test coverage : $totalCoverage %"'
    - |
      if (( $(echo "$totalCoverage $TESTCOVERAGE_THRESHOLD" | awk '{print ($1 > $2)}') )); then
          echo "OK"
      else
          echo "Current test coverage is below threshold. Please add more unit tests or adjust threshold to a lower value."
          echo "Failed"
          exit 1
      fi
  coverage: '/total:\s+\(statements\)\s+(\d+.\d+%)/'
  artifacts:
    when: always
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml
    paths:
      - coverage.out
      - coverage.html
      - coverage.xml

gobuild:vet:
  image: $GO_IMAGE

  script:
    - !reference [".go-tidy", "script"]
    - echo Running go vet ./... from $PWD | tee vet_output.txt
    - echo Processing $(find . -name "*.go" | wc -l) files >> vet_output.txt
    - echo "-----" >> vet_output.txt
    - go vet ./...  2>&1 | tee -a vet_output.txt
  artifacts:
    when: always
    paths:
      - vet_output.txt

golangci-lint:
  image: $GO_IMAGE
  script:
    - echo "Optional linters can be found here -> https://golangci-lint.run/usage/linters"
    - apt update && apt install jq -y
    - curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(go env GOPATH)/bin v1.54.1
    - golangci-lint run --config .golangci.yml --issues-exit-code 0
    - golangci-lint run --config .golangci.yml --out-format code-climate | jq > gl-code-quality-report.json
  artifacts:
    reports:
      codequality: gl-code-quality-report.json
    paths:
      - gl-code-quality-report.json
  allow_failure: false
