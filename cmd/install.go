/*
 *  File: install.go
 *  Copyright © 2023 Lockheed Martin <open.source@lmco.com>
 *
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 */

package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"

	log "github.com/sirupsen/logrus"

	"gitlab.com/hoppr/droppr/pkg/extractors"
	"gitlab.com/hoppr/droppr/pkg/install"
	"gitlab.com/hoppr/droppr/pkg/logging"
	"github.com/spf13/viper"
)

// installCmd represents the install command
var installCmd = &cobra.Command{
	Use:   "install",
	Short: "Install artifacts from a Hoppr bundle",
	Long: `A longer description that spans multiple lines and likely contains examples
 and usage of using your command. For example:
 
 Cobra is a CLI library for Go that empowers applications.
 This application is a tool to generate the needed files
 to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		hopprBundle, _ := cmd.Flags().GetString("bundle")
		fmt.Printf("Beginning install process on bundle %s\n", hopprBundle)

		logfileName, _ := cmd.Flags().GetString("logfile")
		logFile, err := os.OpenFile(logfileName, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0666)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Failed to open log file %s: %s\n", logfileName, err.Error())
			os.Exit(4)
		}
		defer logFile.Close()

		logging.Initialize(logFile)

		config, err := initConfig()
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error reading config file %s: %s\n", viper.ConfigFileUsed(), err.Error())
			os.Exit(1)
		}

		var tempDir string
		tempDir, err = os.MkdirTemp(config.Unpack_Directory, "hoppr_bundle_")
		if err != nil {
			log.Errorf("Error creating unpack directory: %s\n", err.Error())
			fmt.Println("Error creating unpack directory: ", err)
			os.Exit(5)
		}
		defer os.RemoveAll(tempDir)

		if err := extractors.UnpackHopprArtifact(tempDir, hopprBundle); err != nil {
			log.Errorf("Error return unpacking Hoppr Artifact %s: %s\n", hopprBundle, err.Error())
			fmt.Println("Error return unpacking Hoppr Artifact ", hopprBundle, ": ", err)
			os.Exit(2)
		}
		log.Println("Successfully extracted Hoppr bundle ", hopprBundle)

		if err := install.Distribute(tempDir, config); err != nil {
			log.Errorf("Error return distributing packages from %s: %s\n", hopprBundle, err.Error())
			os.Exit(3)
		}

	},
}

func init() {
	rootCmd.AddCommand(installCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// installCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// installCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	installCmd.Flags().StringP("bundle", "b", "", "Hoppr bundle to install (required)")
	if err := installCmd.MarkFlagRequired("bundle"); err !=nil{
		log.Errorf("Error in marking bundle as a required flag: %s\n", err.Error())
		fmt.Println("Error in marking bundle as a required flag: ", err)
		os.Exit(7)
	}
	installCmd.Flags().StringP("logfile", "l", "droppr.log", "Log file location")
}
