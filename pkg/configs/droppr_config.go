/*
 *  File: droppr_config.go
 *  Copyright © 2023 Lockheed Martin <open.source@lmco.com>
 *
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 */

package configs

import (
	"fmt"
	"strings"

	"github.com/golang-demos/chalk"
)

type DropprConfig struct {
	Repos                    []PackageType `yaml:"repos" json:"repos"`
	Num_Workers              int           `yaml:"num_workers" json:"num_workers"`
	Unpack_Directory         string        `yaml:"unpack_directory" json:"unpack_directory"`
	Hoppr_Metadata_Directory string        `yaml:"hoppr_metadata_directory,omitempty" json:"hoppr_metadata_directory,omitempty"`
}

type Artifact struct {
	Path string `yaml:"path" json:"path"`
}

type PackageType struct {
	Purl_Type     string                  `yaml:"purl_type" json:"purl_type"`
	Target_Type   string                  `yaml:"target_type,omitempty" json:"target_type,omitempty"`
	Username      string                  `yaml:"username" json:"username"`
	Password      string                  `yaml:"-" json:"-"`
	Password_Env  string                  `yaml:"password_env" json:"password_env"`
	Regex_Match   string                  `yaml:"regex_match,omitempty" json:"regex_match,omitempty"`
	File_System   *FileSystemTargetType   `yaml:"file_system,omitempty" json:"file_system,omitempty"`
	Local_Install *LocalInstallTargetType `yaml:"local_install,omitempty" json:"local_install,omitempty"`
	Nexus         *NexusTargetType        `yaml:"nexus,omitempty" json:"nexus,omitempty"`
}

type FileSystemTargetType struct {
	Target_Directory string `yaml:"target_directory" json:"target_directory"`
}

type LocalInstallTargetType struct {
	Target_Directory        string   `yaml:"target_directory,omitempty" json:"target_directory,omitempty"`
	Package_Manager_Command []string `yaml:"package_manager_command,omitempty" json:"package_manager_command,omitempty"`
}

type NexusTargetType struct {
	Api_Url                  string `yaml:"api_url" json:"api_url"`
	Repo_Name                string `yaml:"repo_name" json:"repo_name"`
	Docker_Port              int    `yaml:"docker_port,omitempty" json:"docker_port,omitempty"`
	Docker_Protocol          string `yaml:"docker_protocol,omitempty" json:"docker_protocol,omitempty"`
	Docker_Url               string `yaml:"docker_url,omitempty" json:"docker_url,omitempty"`
	Apt_Signing_Keypair_File string `yaml:"apt_signing_keypair_file,omitempty" json:"apt_signing_keypair_file,omitempty"`
}

func (inPkg *DropprConfig) FillInConfigDefaults() error {
	errors_found := false

	for indx, r := range inPkg.Repos {

		if err := inPkg.Repos[indx].FillInDefaults(); err != nil {
			errorMsg := chalk.Red(fmt.Sprintf("ERROR: Unable to fill %s config defaults for %s install %s.",
				r.Purl_Type, r.Target_Type, err)).String()
			fmt.Println(errorMsg)

			errors_found = true
		}
	}
	if errors_found {
		return fmt.Errorf("fill defaults for configuration(s)")
	}
	return nil
}

func (pkg *PackageType) IsTargetTypeFileSystem() bool {
	aTrgType := strings.ToLower(pkg.Target_Type)
	return aTrgType == "filesys" || aTrgType == "file_sys" || aTrgType == "filesystem" || aTrgType == "file_system"
}

func (pkg *PackageType) IsTargetTypeLocalInstall() bool {
	aTrgType := strings.ToLower(pkg.Target_Type)
	return aTrgType == "local" || aTrgType == "local_install" || aTrgType == "localinstall"
}

func (pkg *PackageType) IsTargetTypeNexus() bool {
	return strings.ToLower(pkg.Target_Type) == "nexus"
}

func (pkg *PackageType) FillInDefaults() error {
	numStructs := 0

	// If the 'file_system:' was specified, implies 'target_type: file_system'
	if pkg.File_System != nil {
		numStructs++
		if pkg.Target_Type == "" {
			pkg.Target_Type = "file_system"
		}
		if !pkg.IsTargetTypeFileSystem() {
			err := fmt.Errorf("target type mismatch: file_system vs %s", pkg.Target_Type)
			return err
		}

	}

	// If the 'local_install:' was specified, implies 'target_type: local_install'
	if pkg.Local_Install != nil {
		numStructs++
		if pkg.Target_Type == "" {
			pkg.Target_Type = "local_install"
		}
		if !pkg.IsTargetTypeLocalInstall() {
			err := fmt.Errorf("target type mismatch: local_install vs %s", pkg.Target_Type)
			return err
		}

	}

	// If  'nexus:'  was specified in the droppr config, implies 'target_type: nexus'
	if pkg.Nexus != nil {
		numStructs++
		if pkg.Target_Type == "" {
			pkg.Target_Type = "nexus"
		}
		if !pkg.IsTargetTypeNexus() {
			err := fmt.Errorf("target type mismatch: nexus vs %s", pkg.Target_Type)
			return err
		}
		// If docker port not specified, set to 500
		if pkg.Nexus.Docker_Port == 0 {
			pkg.Nexus.Docker_Port = 5000
		}

	}

	// Target type is specified, but the optional local install struct is nil, then create it
	if pkg.Local_Install == nil && pkg.IsTargetTypeLocalInstall() {
		pkg.Local_Install = new(LocalInstallTargetType)
		numStructs++
	}

	// Return error if config file has multiple target type structures
	// e.g. cannot be both file_system and nexus at the same time.
	if numStructs > 1 {
		err := fmt.Errorf("multiple target types not allowed per package for purl type %s", pkg.Purl_Type)
		return err
	}

	// Return error if target type could not be determined
	if pkg.Target_Type == "" {
		err := fmt.Errorf("unable to determine target type for purl type %s", pkg.Purl_Type)
		return err
	}

	// Return error if we have no way to determine the target type.
	if numStructs == 0 {
		err := fmt.Errorf("missing details for target_type: %s for purl type: %s", pkg.Target_Type, pkg.Purl_Type)
		return err
	}

	return nil

}

// Clones (deep copy) a PackageType
func (inPkg *PackageType) Clone() PackageType {
	clone := *inPkg
	if inPkg.File_System != nil {
		clone.File_System = new(FileSystemTargetType)
		*clone.File_System = *inPkg.File_System
	}
	if inPkg.Local_Install != nil {
		clone.Local_Install = new(LocalInstallTargetType)
		*clone.Local_Install = inPkg.Local_Install.Clone()
	}
	if inPkg.Nexus != nil {
		clone.Nexus = new(NexusTargetType)
		*clone.Nexus = *inPkg.Nexus
	}
	return clone
}

// Clones (deep copy) a Local Install type
func (locInst *LocalInstallTargetType) Clone() LocalInstallTargetType {
	var clone LocalInstallTargetType
	clone.Target_Directory = locInst.Target_Directory

	if locInst.Package_Manager_Command != nil {
		clone.Package_Manager_Command = make([]string, len(locInst.Package_Manager_Command))

		copy(clone.Package_Manager_Command, locInst.Package_Manager_Command)
	}

	return clone
}
