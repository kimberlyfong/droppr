package extractors

import (
	"archive/tar"
	"compress/gzip"
	"fmt"
	"io"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUnpackHopprArtifact(t *testing.T) {
	testCases := map[string]struct{
		bundleName string
		openError error
		tarGzError error
		tarError error
		expected error
	}{
		"tar.gz success": {"bundle.tar.gz", nil, nil, nil, nil},
		"tar success": {"bundle.tar", nil, nil, nil, nil},
		"open failure": {
			"bundle.tar.gz",
			fmt.Errorf("Mock Open Error"),
			nil,
			nil,
			fmt.Errorf("Cowardly failed to open artifact: Mock Open Error"),
		},
		"tar.gz failure": {
			"bundle.tar.gz",
			nil,
			fmt.Errorf("Mock TarGz Error"),
			nil,
			fmt.Errorf("Mock TarGz Error"),
		},
		"tar failure": {
			"bundle.tar",
			nil,
			nil,
			fmt.Errorf("Mock Tar Error"),
			fmt.Errorf("Mock Tar Error"),
		},
		"bad file extension": {
			"bundle.txt",
			nil,
			nil,
			nil,
			fmt.Errorf("No extraction defined for bundle.txt"),
		},
	}

	
	for key, tc := range testCases {
		mockOpen := func(name string) (*os.File, error) {
			return nil, tc.openError
		}

		mockExtTarGz := func(gzipStream io.Reader, outdir string) error {
			return tc.tarGzError
		}

		mockExtTar := func(uncompressedStream io.Reader, outdir string) error {
			return tc.tarError
		}

		actual := unpackHopprArtifact("OUTDIR", tc.bundleName, mockOpen, mockExtTarGz, mockExtTar)
		assert.Equal(t, tc.expected, actual, "Test Case: " + key)
	}
}
func TestExtractTarGz(t *testing.T) {
	testCases := map[string]struct{
		newReaderError error
		tarError error
		expected error
	}{
		"success": {nil, nil, nil},
		"new reader failure": {
			fmt.Errorf("Mock NewReader Error"),
			nil,
			fmt.Errorf("Mock NewReader Error"),
		},
		"extract tar failure": {
			nil,
			fmt.Errorf("Mock ExtractTar Error"),
			fmt.Errorf("Mock ExtractTar Error"),
		},
	}

	
	for key, tc := range testCases {
		mockNewReader := func(r io.Reader) (*gzip.Reader, error) {
			return nil, tc.newReaderError
		}

		mockExtTar := func(uncompressedStream io.Reader, outdir string) error {
			return tc.tarError
		}

		actual := extractTarGz(nil, "OUTDIR", mockNewReader, mockExtTar)
		assert.Equal(t, tc.expected, actual, "Test Case: " + key)
	}
}


func TestExtractTar(t *testing.T) {
	testCases := map[string]struct{
		fileNames []string
		mkdirError error
		createError error
		copyError error
		expectedError error
		expectedCopyCount int
		expectedMkdirCount int
	}{
		"success": {
			[]string{"alpha", "beta/", "gamma"},
			nil, nil, nil,
			nil, 2, 1,
		},
		"bad next": {
			[]string{"alpha", "beta/", "ERRgamma"},
			nil, nil, nil,
			fmt.Errorf("ExtractTar: Next() failed: %w", fmt.Errorf("Mock TarReader Next error")), 1, 1,
		},
		"bad mkdir": {
			[]string{"alpha", "beta/", "gamma"},
			fmt.Errorf("Mock mkdir error"), nil, nil,
			fmt.Errorf("ExtractTar: Mkdir() failed: %w", fmt.Errorf("Mock mkdir error")), 1, 1,
		},
		"bad create": {
			[]string{"alpha", "beta/", "gamma"},
			nil, fmt.Errorf("Mock create error"), nil,
			fmt.Errorf("ExtractTar: Create() failed: %w", fmt.Errorf("Mock create error")), 0, 0,
		},
		"bad copy": {
			[]string{"alpha", "beta/", "gamma"},
			nil, nil, fmt.Errorf("Mock copy error"), 
			fmt.Errorf("ExtractTar: Copy() failed: %w", fmt.Errorf("Mock copy error")), 1, 0,
		},
		"bad type": {
			[]string{"alpha", "beta/", "gamma?"},
			nil, nil, nil, 
			fmt.Errorf("ExtractTar: uknown type: 111111 in gamma?"), 1, 1,
		},
	}

	
	for key, tc := range testCases {
		mkdirCount := 0
		copyCount := 0
		fileptr := 0

		mockNext := func() (*tar.Header, error) {
			if fileptr >= len(tc.fileNames) {
				return nil, io.EOF
			}
		
			fn := tc.fileNames[fileptr]
		
			var ty byte = tar.TypeReg
			if strings.HasSuffix(fn, "/") {
				ty = tar.TypeDir
			}
			if strings.HasSuffix(fn, "?") {
				ty = '?'
			}

			header := tar.Header{Name: fn, Typeflag: ty}
			fileptr++
		
			var err error = nil
			if strings.HasPrefix(fn, "ERR") {
				err = fmt.Errorf("Mock TarReader Next error")
			}
			return &header, err
		}
		
		mockMkDir := func(path string, perm os.FileMode) error {
			mkdirCount++
			return tc.mkdirError
		}

		mockCreate := func(name string) (*os.File, error) {
			return nil, tc.createError
		}

		mockCopy := func(dst io.Writer, src io.Reader) (int64, error) {
			copyCount++
			return 0, tc.copyError
		}

		actual := extractTar(nil, "OUTDIR", mockNext, mockMkDir, mockCreate, mockCopy)
		assert.Equal(t, tc.expectedError, actual, "Return for Test Case: " + key)
		assert.Equal(t, tc.expectedMkdirCount, mkdirCount, "Mkdir Count for Test Case: " + key)
		assert.Equal(t, tc.expectedCopyCount, copyCount, "Copy Count for Test Case: " + key)
	}
}
