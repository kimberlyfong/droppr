/*
 *  File: raw_dist_test.go
 *  Copyright © 2023 Lockheed Martin <open.source@lmco.com>
 *
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 */

package install

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"
	"testing/fstest"

	"github.com/stretchr/testify/assert"
	"gitlab.com/hoppr/droppr/pkg/nexus"
)

func TestRawInstallNexus(t *testing.T) {
	placeholderComp := testComponent("generic")
	testCases := map[string]struct {
		fileNames []string
		uploadRc  []int
		openError bool
		expected  Result
	}{
		"upload multiple files": {
			[]string{"some-image.img", "some-file.sh", "some-json.json"},
			[]int{200, 200, 200},
			false,
			Result{true, placeholderComp, ""},
		},
		"upload failure": {
			[]string{"some-image.img", "some-file.sh", "some-json.json"},
			[]int{400, 400, 400},
			false,
			Result{false, placeholderComp, "Error response from upload HTTP call: 400 Bad Request"},
		},
	}

	for key, tc := range testCases {
		serverCallIndex := 0
		server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(tc.uploadRc[serverCallIndex])
			serverCallIndex++

		}))
		defer server.Close()

		dist := rawDist{*NewBaseDist("generic", testConfig("nexus", server.URL), "basedirectory")}
		comp := testComponent("pkg:generic/name/space/TestGeneric@1.2.3")

		testNexus := nexus.Server{Url: server.URL}

		mockFileSys := fstest.MapFS{}
		for _, name := range tc.fileNames {
			if strings.HasSuffix(name, "/") {
				mockFileSys[strings.TrimRight(name, "/")] = &fstest.MapFile{Mode: os.ModeDir}
			} else {
				mockFileSys[name] = &fstest.MapFile{}
			}
		}

		mockOpen := func(name string) (io.Reader, error) {
			return bytes.NewReader([]byte("Data from file " + name)), nil
		}

		result := dist.installNexus(comp, testNexus, "test_generic_repo", mockOpen)

		assert.Equal(t, tc.expected.Success, result.Success, "Result Status mis-match, Test Case: "+key)
		assert.Equal(t, comp, result.Component, "Component mis-match, Test Case: "+key)
		assert.Equal(t, tc.expected.Message, result.Message, "Message mis-match, Test Case: "+key)
	}
}

func TestRawCheckNexusRepository(t *testing.T) {
	testCases := map[string]struct {
		getRepoRc     int
		getRepoJson   string
		createRepoRc  int
		expectedError error
	}{
		"got repo": {
			200,
			"[{\"name\": \"good-repo\", \"format\": \"raw\", \"type\": \"hosted\"}]",
			200,
			nil,
		},

		"invalid repo": {
			200,
			"[{\"name\": \"test_raw_repo\", \"format\": \"maven2\", \"type\": \"hosted\"}]",
			200,
			fmt.Errorf("Repository test_raw_repo has format 'maven2', format 'raw' requested"),
		},
		"create new repo": {
			404,
			"[{\"name\": \"good-repo\", \"format\": \"raw\", \"type\": \"hosted\"}]",
			200,
			nil,
		},
		"get repo failure": {
			401,
			"[{\"name\": \"good-repo\", \"format\": \"raw\", \"type\": \"hosted\"}]",
			200,
			fmt.Errorf("Error response from HTTP call: 401 Unauthorized"),
		},
		"create repo failure": {
			404,
			"[{\"name\": \"good-repo\", \"format\": \"raw\", \"type\": \"hosted\"}]",
			401,
			fmt.Errorf("Error response from HTTP call to build repository: 401 Unauthorized"),
		},
	}

	for testcasename, tc := range testCases {

		server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if strings.HasSuffix(fmt.Sprint(r.URL), "/hosted") {
				w.WriteHeader(tc.createRepoRc)
			} else {
				w.WriteHeader(tc.getRepoRc)
				if _, err := w.Write([]byte(tc.getRepoJson)); err != nil {
					fmt.Printf("Error while writing RepoJson, %s", err)
				}
			}
		}))
		defer server.Close()

		dist := rawDist{*NewBaseDist("generic", testConfig("nexus", server.URL), "basedirectory")}
		testNexus := nexus.Server{Url: server.URL}
		err := dist.CheckNexusRepository(testNexus, "test_raw_repo")
		assert.Equal(t, fmt.Sprint(tc.expectedError), fmt.Sprint(err), "Error mis-match, Test Case: "+testcasename)
	}
}
