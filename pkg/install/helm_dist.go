/*
 *  File: helm_dist.go
 *  Copyright © 2023 Lockheed Martin <open.source@lmco.com>
 *
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 */

package install

import (
	"fmt"
	"io"
	"os"

	cdx "github.com/CycloneDX/cyclonedx-go"

	"gitlab.com/hoppr/droppr/pkg/nexus"
	"gitlab.com/hoppr/droppr/pkg/utils"
)

type helmDist struct {
	baseDist
}

// no install local for helm

// func (self *helmDist) InstallFilesys(comp cdx.Component) Result {
// 	self.Log().Println("Processing file system install for helm files")
// 	return self.baseDist.InstallFilesys(comp)
// }

func (self *helmDist) InstallNexus(comp cdx.Component) Result {
	repoConfig, err := self.GetRepoConfig(comp)
	if err != nil {
		msg := fmt.Sprintf("Error installing %s component to Nexus: %s", comp.PackageURL, err)
		self.Log().Errorf(msg)
		return Result{false, comp, msg}
	}

	repoName := repoConfig.Nexus.Repo_Name
	if repoName == "" {
		repoName = "droppr_helm"
	}

	targetNexus := nexus.Server{
		Url:      repoConfig.Nexus.Api_Url,
		Username: repoConfig.Username,
		Password: repoConfig.Password,
	}

	err = self.CheckNexusRepository(targetNexus, repoName)
	if err != nil {
		return Result{false, comp, err.Error()}
	}

	return self.installNexus(comp, targetNexus, repoName, os.ReadDir, utils.OpenFileAsReader)
}

func (self *helmDist) installNexus(
	comp cdx.Component,
	targetNexus nexus.Server,
	repoName string,
	readDirFunc func(name string) ([]os.DirEntry, error),
	openFunc func(name string) (io.Reader, error),
) Result {

	purl := ParsePurl(comp.PackageURL)
	fileName := purl.Name
	data := map[string]interface{}{}
	dir := self.baseDir + string(os.PathSeparator) + getCollectionData(comp)[collectionDirectory]
	files, err := readDirFunc(dir)
	if err != nil {
		return Result{false, comp, err.Error()}
	}

	for _, f := range files {
		fptr, err := openFunc(dir + string(os.PathSeparator) + f.Name())
		if err != nil {
			return Result{false, comp, err.Error()}
		}

		data["helm.asset1"] = fileName
		err = targetNexus.Upload(f.Name(), "helm.asset1", fptr, repoName, data)
		if err != nil {
			return Result{false, comp, err.Error()}
		}
	}

	return Result{true, comp, ""}
}

func (self *helmDist) CheckNexusRepository(targetNexus nexus.Server, repoName string) error {
	targetNexus.GetLock().Lock()
	defer targetNexus.GetLock().Unlock()

	repo, err := targetNexus.GetRepository(repoName)
	if err != nil {
		return err
	}

	additionalParams := map[string]interface{}{
		"component": map[string]bool{
			"proprietaryComponents": true,
		},
	}

	if repo != nil {
		err := self.ValidateNexusRepository(repo, "helm", additionalParams)
		if err != nil {
			return err
		}
	} else {
		err := targetNexus.CreateRepository(repoName, "helm", "helm", additionalParams)
		if err != nil {
			return err
		}
	}

	return nil
}
