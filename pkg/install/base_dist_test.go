/*
 *  File: base_dist_test.go
 *  Copyright © 2023 Lockheed Martin <open.source@lmco.com>
 *
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 */

package install

import (
	"fmt"
	"net/url"
	"testing"

	cdx "github.com/CycloneDX/cyclonedx-go"

	"github.com/stretchr/testify/assert"

	"gitlab.com/hoppr/droppr/pkg/configs"
	"gitlab.com/hoppr/droppr/pkg/nexus"
)

func testComponent(purl string) cdx.Component {
	comp := new(cdx.Component)

	comp.PackageURL = purl
	comp.Properties = new([]cdx.Property)
	*comp.Properties = []cdx.Property{
		{Name: collectionDirectory, Value: "type/" + url.QueryEscape("http://my-repo") + "/the/collection/dir"},
		{Name: collectionPlugin, Value: "testPlugin"},
		{Name: "something-to-ignore", Value: "42"},
		{Name: collectionRepository, Value: "http://my-repo"},
	}

	return *comp
}

func TestGetCollectionData(t *testing.T) {
	comp := testComponent("pkg:generic/whatever")

	collectionData := getCollectionData(comp)
	assert.Equal(t, 3, len(collectionData))
	assert.Equal(t, "type/http%3A%2F%2Fmy-repo/the/collection/dir", collectionData[collectionDirectory])
	assert.Equal(t, "testPlugin", collectionData[collectionPlugin])
	assert.Equal(t, "http://my-repo", collectionData[collectionRepository])
	assert.Equal(t, "", collectionData[collectionTimetag])
}

func testConfig(installType string, location string) *configs.DropprConfig {
	c := new(configs.DropprConfig)
	c.Repos = []configs.PackageType{}

	for idx, purl_type := range []string{"pypi", "maven", "pypi", "docker", "generic", "git", "helm", "rpm", "apt", "npm", "nuget"} {
		p := configs.PackageType{}
		p.Purl_Type = purl_type
		p.Target_Type = installType
		if p.IsTargetTypeNexus() {
			p.Nexus = new(configs.NexusTargetType)
			if location != "" {
				p.Nexus.Api_Url = location + fmt.Sprint(idx+1)
			}
		} else if p.IsTargetTypeFileSystem() {
			p.File_System = new(configs.FileSystemTargetType)
			if location != "" {
				p.File_System.Target_Directory = location + fmt.Sprint(idx+1)
			}
		} else if p.IsTargetTypeLocalInstall() {
			p.Local_Install = new(configs.LocalInstallTargetType)
			if location != "" {
				p.Local_Install.Target_Directory = location + fmt.Sprint(idx+1)
			}
		}

		c.Repos = append(c.Repos, p)
	}

	return c
}

func TestInstallLocal(t *testing.T) {
	dist := NewBaseDist("testPurl", nil, "base_directory")
	comp := testComponent("testPurl")
	actual := dist.InstallLocal(comp)

	assert.Equal(t, Result{false, comp, "InstallLocal function not defined for testPurl"}, actual)
}

func TestInstallNexus(t *testing.T) {
	dist := NewBaseDist("testPurl", nil, "base_directory")
	comp := testComponent("testPurl")
	actual := dist.InstallNexus(comp)

	assert.Equal(t, Result{false, comp, "InstallNexus function not defined for testPurl"}, actual)
}

func TestGetRepoConfig(t *testing.T) {
	test_config := testConfig("filesys", "./loc")

	backrefExpected := configs.FileSystemTargetType{Target_Directory: "my/repo/loc2/br-repo/br-my"}

	testCases := map[string]struct {
		purlType        string
		regex           string
		locString       string
		expected_result *configs.PackageType
		expected_error  error
	}{
		"pypi":         {"pypi", "", "./loc2", &test_config.Repos[0], nil},
		"good regex":   {"maven", "my-repo", "./loc2", &test_config.Repos[1], nil},
		"no match":     {"maven", "https://my-repo", "./loc2", nil, fmt.Errorf("Unable to locate configuration for Purl type maven matching repository http://my-repo")},
		"bad regex":    {"maven", "my(repo", "./loc2", nil, fmt.Errorf("Problem with regex: error parsing regexp: missing closing ): `my(repo`")},
		"not-found":    {"badPurl", "", "./loc2", nil, fmt.Errorf("Unable to locate configuration for Purl type badPurl matching repository http://my-repo")},
		"backref-test": {"maven", "(my)-(repo)", "$1/$2/loc2/br-$2/br-$1", &configs.PackageType{Purl_Type: "maven", Regex_Match: "(my)-(repo)", Target_Type: "filesys", File_System: &backrefExpected}, nil},
	}

	for key, tc := range testCases {
		component := testComponent("pkg:" + tc.purlType + "/filename@1.2.3")
		test_config.Repos[1].Regex_Match = tc.regex
		test_config.Repos[1].File_System.Target_Directory = tc.locString
		dist := NewBaseDist(tc.purlType, test_config, "base_directory")
		actual_result, actual_error := dist.GetRepoConfig(component)
		assert.Equal(t, tc.expected_result, actual_result, "Result, Test Case: "+key)
		assert.Equal(t, tc.expected_error, actual_error, "Error, Test Case: "+key)
	}
}

func TestSubBackrefs(t *testing.T) {
	testCases := map[string]struct {
		input_string    string
		backrefs        []string
		expected_result string
	}{
		"simple":      {"abc$1d", []string{}, "abc$1d"},
		"two changes": {"$2ab$3c$$$1d$", []string{"", "-alpha-", "-beta-"}, "-beta-ab$3c$-alpha-d$"},
		"Sub in $2":   {"ab$1-$2c$d$$e", []string{"", "$2", "-stuff-"}, "ab$2--stuff-c$d$e"},
	}

	for key, tc := range testCases {
		actual_result := subBackRefs(tc.input_string, tc.backrefs)
		assert.Equal(t, tc.expected_result, actual_result, "Result, Test Case: "+key)
	}
}

func TestInstallFilesys(t *testing.T) {
	placeholderComp := testComponent("testPurl")
	testCases := map[string]struct {
		purlType    string
		targetLoc   string
		copy_result error
		expected    Result
	}{
		"success": {"pypi", "./loc", nil, Result{true, placeholderComp, ""}},
		"purl-not-found": {"badPurl", "./loc", nil,
			Result{false, placeholderComp, "Error installing pkg:badPurl/filename@1.2.3 component to local file system: Unable to locate configuration for Purl type badPurl matching repository http://my-repo"}},
		"bad-copy": {"pypi", "./loc", fmt.Errorf("Copy failed"),
			Result{false, placeholderComp, "Unable to copy from base_directory/type/http%3A%2F%2Fmy-repo/the/collection/dir to ./loc1/type/http%3A%2F%2Fmy-repo/the/collection/dir, Copy failed."}},
		"no-tgt-loc": {"pypi", "", nil, Result{false, placeholderComp, "Filesystem install target directory not specified"}},
	}

	for key, tc := range testCases {
		test_config := testConfig("filesys", tc.targetLoc)
		mockCopy := func(string, string) error {
			return tc.copy_result
		}

		component := testComponent("pkg:" + tc.purlType + "/filename@1.2.3")
		dist := NewBaseDist(tc.purlType, test_config, "base_directory")
		actual := dist.installFilesys(component, mockCopy)
		tc.expected.Component = component

		assert.Equal(t, tc.expected, actual, "Error, Test Case: "+key)
	}
}

func TestValidateNexusRepo(t *testing.T) {
	testCases := map[string]struct {
		repoFormat string
		repoType   string
		expected   error
	}{
		"good":       {"pypi", "hosted", nil},
		"bad-format": {"maven", "hosted", fmt.Errorf("Repository bad-format has format 'maven', format 'pypi' requested")},
		"bad-type":   {"pypi", "proxy", fmt.Errorf("Repository bad-type has type 'proxy', only hosted repositories are supported")},
	}

	test_config := testConfig("filesys", "./loc")
	dist := NewBaseDist("pypi", test_config, "base_directory")

	for key, tc := range testCases {
		test_repo := nexus.Repository{
			Name:   key,
			Format: tc.repoFormat,
			Type:   tc.repoType,
			Url:    "my-nexus.com/repository/test-repo",
		}

		actual := dist.ValidateNexusRepository(&test_repo, "pypi", map[string]interface{}{})

		assert.Equal(t, tc.expected, actual, "Error, Test Case: "+key)
	}
}
