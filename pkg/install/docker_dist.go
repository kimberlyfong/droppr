/*
 *  File: docker_dist.go
 *  Copyright © 2023 Lockheed Martin <open.source@lmco.com>
 *
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 */

package install

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"os"
	"regexp"
	"strings"

	cdx "github.com/CycloneDX/cyclonedx-go"
	"github.com/google/go-containerregistry/pkg/authn"
	"github.com/google/go-containerregistry/pkg/crane"
	v1 "github.com/google/go-containerregistry/pkg/v1"

	dockerTypes "github.com/docker/docker/api/types"
	dockerClient "github.com/docker/docker/client"
	"gitlab.com/hoppr/droppr/pkg/nexus"
	"gitlab.com/hoppr/droppr/pkg/utils"
)

type dockerDist struct {
	baseDist
}

func (self *dockerDist) InstallFilesys(comp cdx.Component) Result {
	return self.installFilesys(comp, os.MkdirAll, utils.CopyFile)
}

func (self *dockerDist) installFilesys(
	comp cdx.Component,
	mkdirFunc func(path string, perm os.FileMode) error,
	copyFunc func(string, string) error,
) Result {

	collectionData := getCollectionData(comp)

	repoConfig, err := self.GetRepoConfig(comp)
	if err != nil {
		msg := fmt.Sprintf("Error installing %s component to local file system: %s", comp.PackageURL, err)
		self.Log().Error(msg)
		return Result{false, comp, msg}
	}

	if repoConfig.File_System.Target_Directory == "" {
		msg := "Filesystem target directory not specified"
		self.Log().Error(msg)
		return Result{false, comp, msg}
	}

	fileName := self.getFileName(comp)
	src := self.baseDir + string(os.PathSeparator) + collectionData[collectionDirectory] + string(os.PathSeparator) + fileName
	tgtDir := repoConfig.File_System.Target_Directory + string(os.PathSeparator) + collectionData[collectionDirectory]
	tgt := tgtDir + string(os.PathSeparator) + fileName

	err = mkdirFunc(tgtDir, os.ModePerm)
	if err != nil {
		msg := fmt.Sprintf("Unable to create directory: %s", tgtDir)
		self.Log().Error(msg)
		return Result{false, comp, msg}

	}

	err = copyFunc(src, tgt)
	if err == nil {
		return Result{true, comp, ""}
	}

	msg := fmt.Sprintf("Unable to copy from %s to %s, %s.", src, tgt, err)
	self.Log().Error(msg)
	return Result{false, comp, msg}
}

func (self *dockerDist) getFileName(comp cdx.Component) string {
	purl := ParsePurl(comp.PackageURL)
	collector := getCollectionData(comp)[collectionPlugin]
	_, version, _ := strings.Cut(collector, ":")

	if utils.VersionAtLeast(version, "1.8.6") {
		return purl.Name + "@" + purl.Version
	}
	return purl.Name + "_" + purl.Version
}

func (self *dockerDist) InstallLocal(comp cdx.Component) Result {
	dir := self.baseDir + string(os.PathSeparator) + getCollectionData(comp)[collectionDirectory]
	fqfn := dir + string(os.PathSeparator) + self.getFileName(comp)
	fptr, err := utils.OpenFileAsReader(fqfn)
	if err != nil {
		return Result{false, comp, err.Error()}
	}

	cli, err := dockerClient.NewClientWithOpts(dockerClient.FromEnv)
	if err != nil {
		return Result{false, comp, err.Error()}
	}

	return self.installLocal(comp, fptr, cli.ImageLoad, cli.ImageTag)
}

func (self *dockerDist) installLocal(
	comp cdx.Component,
	filePtr io.Reader,
	imageLoadFunc func(ctx context.Context, input io.Reader, quiet bool) (dockerTypes.ImageLoadResponse, error),
	imageTagFunc func(ctx context.Context, imageID, ref string) error,
) Result {

	result, err := imageLoadFunc(context.Background(), filePtr, false)
	if err != nil {
		return Result{false, comp, err.Error()}
	}
	defer result.Body.Close()

	buf := new(bytes.Buffer)
	if _, err := buf.ReadFrom(result.Body); err != nil {
		return Result{false, comp, err.Error()}
	}
	resultString := buf.String()

	self.Log().Printf("Output from Image Load: JSON: %t,\n   %s", result.JSON, resultString)

	purl := ParsePurl(comp.PackageURL)
	tag := purl.Name + ":" + purl.Version

	re := regexp.MustCompile(`(sha.*?:[0-9a-f]*)`)
	matches := re.FindStringSubmatch(resultString)
	if len(matches) < 2 {
		return Result{false, comp, "Unable to determine loaded image id"}
	}

	err = imageTagFunc(context.Background(), matches[1], tag)
	if err != nil {
		return Result{false, comp, err.Error()}
	}

	return Result{true, comp, ""}
}

func (self *dockerDist) InstallNexus(comp cdx.Component) Result {
	repoConfig, err := self.GetRepoConfig(comp)
	if err != nil {
		msg := fmt.Sprintf("Error installing %s component to Nexus: %s", comp.PackageURL, err)
		self.Log().Error(msg)
		return Result{false, comp, msg}
	}

	repoName := repoConfig.Nexus.Repo_Name
	if repoName == "" {
		repoName = "droppr_docker"
	}

	targetNexus := nexus.Server{
		Url:             repoConfig.Nexus.Api_Url,
		Username:        repoConfig.Username,
		Password:        repoConfig.Password,
		Docker_Protocol: repoConfig.Nexus.Docker_Protocol,
		Docker_Port:     repoConfig.Nexus.Docker_Port,
		Docker_Url:      repoConfig.Nexus.Docker_Url,
	}

	err = self.CheckNexusRepository(targetNexus, repoName)
	if err != nil {
		return Result{false, comp, err.Error()}
	}

	return self.installNexus(comp, targetNexus, crane.Load, crane.Push)
}

func (self *dockerDist) installNexus(
	comp cdx.Component,
	targetNexus nexus.Server,
	imageLoadFunc func(path string, opt ...crane.Option) (v1.Image, error),
	imagePushFunc func(img v1.Image, dst string, opt ...crane.Option) error,
) Result {

	purl := ParsePurl(comp.PackageURL)

	dir := self.baseDir + string(os.PathSeparator) + getCollectionData(comp)[collectionDirectory]
	path := dir + string(os.PathSeparator) + self.getFileName(comp)

	img, err := imageLoadFunc(path)
	if err != nil {
		return Result{false, comp, "Unable to load docker image from tarball: " + err.Error()}
	}

	dst := targetNexus.GetDockerRepo() + "/" + purl.Namespace + "/" + purl.Name + ":" + purl.Version
	self.Log().Printf("Attempting docker push to %s\n", dst)
	opts := []crane.Option{}
	opts = append(opts, crane.WithAuth(&authn.Basic{Username: targetNexus.Username, Password: targetNexus.Password}))
	// TODO: Should the following line use Docker_Protocol?
	if strings.HasPrefix(targetNexus.Url, "http://") {
		self.Log().Println("Setting target repository as insecure")
		opts = append(opts, crane.Insecure)
	}

	err = imagePushFunc(img, dst, opts...)
	if err != nil {
		return Result{false, comp, err.Error()}
	}

	return Result{true, comp, ""}
}

func (self *dockerDist) ValidateNexusRepository(
	repo *nexus.Repository,
	format string,
	additionalParams map[string]interface{},
) error {

	dockerData := additionalParams["docker"].(map[string]interface{})

	reqPort, ok := dockerData["httpsPort"].(int)
	if ok {
		if repo.Docker["httpsPort"] == nil {
			err := fmt.Errorf("Repository '%s' already exists, but has no HTTPS port assigned.  Requested port is %d",
				repo.Name, reqPort)
			return err
		}
		if reqPort != int(repo.Docker["httpsPort"].(float64)+0.5) {
			err := fmt.Errorf("Repository '%s' already exists, using HTTPS port %d, which is not the specified docker port %d",
				repo.Name, repo.Docker["httpsPort"], reqPort)
			return err
		}
	}

	reqPort, ok = dockerData["httpPort"].(int)
	if ok {
		if repo.Docker["httpPort"] == nil {
			err := fmt.Errorf("Repository '%s' already exists, but has no HTTP port assigned.  Requested port is %d",
				repo.Name, reqPort)
			return err
		}
		if reqPort != int(repo.Docker["httpPort"].(float64)+0.5) {
			err := fmt.Errorf("Repository '%s' already exists, using HTTP port %.0f, which is not the specified docker port %d",
				repo.Name, repo.Docker["httpPort"], reqPort)
			return err
		}
	}

	return self.baseDist.ValidateNexusRepository(repo, format, additionalParams)
}

func (self *dockerDist) CheckNexusRepository(targetNexus nexus.Server, repoName string) error {

	// If the user supplied a docker url, we assume that the repository is valid and exists
	if targetNexus.Docker_Url != "" {
		return nil
	}

	targetNexus.GetLock().Lock()
	defer targetNexus.GetLock().Unlock()

	if repoName != strings.ToLower(repoName) {
		return fmt.Errorf("'%s' is not a valid Docker repository name, must be all lower case", repoName)
	}

	repo, err := targetNexus.GetRepository(repoName)
	if err != nil {
		return err
	}

	aProtocol, err := targetNexus.GetDockerProtocol()
	if err != nil {
		return err
	}

	portType := "httpPort"
	if aProtocol == "https" {
		portType = "httpsPort"
	}

	additionalParams := map[string]interface{}{
		"docker": map[string]interface{}{
			"v1Enabled":      true,
			"forceBasicAuth": false,
			portType:         targetNexus.Docker_Port,
		},
	}
	if repo != nil {
		err := self.ValidateNexusRepository(repo, "docker", additionalParams)
		if err != nil {
			return err
		}
		self.Log().Printf("Valid docker repository '%s' found", repo)
	} else {
		err := targetNexus.SetRealm("DockerToken")
		if err != nil {
			return err
		}
		err = targetNexus.CreateRepository(repoName, "docker", "docker", additionalParams)
		if err != nil {
			return err
		}
		self.Log().Printf("Docker repository '%s' created", repo)
	}

	return nil
}
