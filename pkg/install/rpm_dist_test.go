/*
 *  File: rpm_dist_test.go
 *  Copyright © 2023 Lockheed Martin <open.source@lmco.com>
 *
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 */

package install

import (
	"bytes"
	"fmt"
	"io"
	"io/fs"
	"net/http"
	"net/http/httptest"
	"os"
	"os/exec"
	"strings"
	"testing"
	"testing/fstest"

	"github.com/stretchr/testify/assert"
	"gitlab.com/hoppr/droppr/pkg/configs"
	"gitlab.com/hoppr/droppr/pkg/nexus"
)

func TestRpmInstallNexus(t *testing.T) {
	testCases := map[string]struct {
		fileNames []string
		uploadRc  int
		openError bool
		expected  error
	}{
		"good": {
			[]string{"file-1.rpm", "testrpm_1.2.3.noarch.rpm", "file-3.rpm"},
			200,
			false,
			nil,
		},
		"upload failure": {
			[]string{"file-1.rpm", "testrpm_1.2.3.noarch.rpm", "file-3.rpm"},
			400,
			false,
			fmt.Errorf("Error response from upload HTTP call: 400 Bad Request"),
		},
		"read error": {
			[]string{"file-1.rpm", "testrpm_1.2.3.noarch.rpm", "file-3.rpm"},
			200,
			true,
			fmt.Errorf("Mock Open Error"),
		},
		"dir read error": {
			[]string{"ERRfile--1.rpm", "ERRfile-2.rpm", "ERRfile-3.rpm"},
			200,
			false,
			fmt.Errorf("Mock Directory Read Error"),
		},
	}

	for key, tc := range testCases {
		server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(tc.uploadRc)
		}))

		dist := rpmDist{*NewBaseDist("rpm", testConfig("nexus", server.URL), "basedirectory")}
		comp := testComponent("pkg:rpm/name/space/testrpm@1.2.3")

		testNexus := nexus.Server{Url: server.URL}

		mockFileSys := fstest.MapFS{}
		for _, name := range tc.fileNames {
			if strings.HasSuffix(name, "/") {
				mockFileSys[strings.TrimRight(name, "/")] = &fstest.MapFile{Mode: os.ModeDir}
			} else {
				mockFileSys[name] = &fstest.MapFile{}
			}
		}

		mockReadDir := func(name string) ([]os.DirEntry, error) {
			content := []os.DirEntry{}
			for name := range mockFileSys {
				if strings.HasPrefix(name, "ERR") {
					return content, fmt.Errorf("Mock Directory Read Error")
				}
				fi, _ := mockFileSys.Stat(name)
				content = append(content, fs.FileInfoToDirEntry(fi))
			}
			return content, nil
		}

		mockOpen := func(name string) (io.Reader, error) {
			if tc.openError {
				return bytes.NewReader([]byte("")), fmt.Errorf("Mock Open Error")
			}
			return bytes.NewReader([]byte("Data from file " + name)), nil
		}
		mockTargetDirString := "SomeFilePath"

		err := dist.installNexus(comp, testNexus, "test_rpm_repo", mockReadDir, mockOpen, mockTargetDirString)

		assert.Equal(t, fmt.Sprint(tc.expected), fmt.Sprint(err), "Result Status mis-match, Test Case: "+key)
	}
}

func TestRpmCheckNexusRepository(t *testing.T) {
	testCases := map[string]struct {
		getRepoRc    int
		getRepoJson  string
		createRepoRc int
		expected     error
	}{
		"got repo": {
			200,
			"[{\"name\": \"test-rpm-repo\", \"format\": \"yum\", \"type\": \"hosted\"}]",
			200,
			nil,
		},
		"invalid repo": {
			200,
			"[{\"name\": \"test-rpm-repo\", \"format\": \"pypi\", \"type\": \"hosted\"}]",
			200,
			fmt.Errorf("Repository test-rpm-repo has format 'pypi', format 'yum' requested"),
		},
		"create repo": {
			404,
			"[{\"name\": \"good-repo\", \"format\": \"yum\", \"type\": \"hosted\"}]",
			200,
			nil,
		},
		"get repo failure": {
			401,
			"[{\"name\": \"good-repo\", \"format\": \"yum\", \"type\": \"hosted\"}]",
			200,
			fmt.Errorf("Error response from HTTP call: 401 Unauthorized"),
		},
		"create repo failure": {
			404,
			"[{\"name\": \"good-repo\", \"format\": \"yum\", \"type\": \"hosted\"}]",
			401,
			fmt.Errorf("Error response from HTTP call to build repository: 401 Unauthorized"),
		},
	}

	for key, tc := range testCases {
		server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if strings.HasSuffix(fmt.Sprint(r.URL), "/hosted") {
				w.WriteHeader(tc.createRepoRc)
			} else {
				w.WriteHeader(tc.getRepoRc)
				if _, err := w.Write([]byte(tc.getRepoJson)); err != nil {
					fmt.Printf("Error while writing RepoJson, %s", err)
				}
			}
		}))
		defer server.Close()

		dist := rpmDist{*NewBaseDist("rpm", testConfig("nexus", server.URL), "basedirectory")}

		testNexus := nexus.Server{Url: server.URL}

		testParam := float64(5)

		err := dist.CheckNexusRepository(testNexus, "test-rpm-repo", testParam)

		assert.Equal(t, fmt.Sprint(tc.expected), fmt.Sprint(err), "Error mis-match, Test Case: "+key)
	}
}

func TestRpmCheckNexusGroupRepository(t *testing.T) {
	testCases := map[string]struct {
		getRepoRc         int
		getRepoJson       string
		mock_group_exists bool
		updateGroupRepoRc int
		createGroupRepoRc int
		expected          error
	}{
		"got repo": {
			200,
			"[{\"name\": \"test-rpm-repo\", \"format\": \"yum\", \"type\": \"group\"}]",
			false,
			204,
			200,
			nil,
		},
		"create group repo": {
			404,
			"[{\"name\": \"create-good-repo\", \"format\": \"yum\", \"type\": \"hosted\"}]",
			true,
			200,
			200,
			nil,
		},
		"get repo failure": {
			401,
			"[{\"name\": \"unauthorized\", \"format\": \"yum\", \"type\": \"hosted\"}]",
			false,
			401,
			401,
			fmt.Errorf("Error response from HTTP call: 401 Unauthorized"),
		},
		"create group repo failure": {
			200,
			"[{\"name\": \"group failure\", \"format\": \"yum\", \"type\": \"hosted\"}]",
			false,
			401,
			401,
			fmt.Errorf("Error response from HTTP call to build/update group repository: 401 Unauthorized"),
		},
	}

	mock_group_name := "test-rpm-repo"

	for key, tc := range testCases {
		server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

			if strings.HasSuffix(fmt.Sprint(r.URL), "/group") {
				if tc.mock_group_exists {
					w.WriteHeader(tc.updateGroupRepoRc)
				} else {
					w.WriteHeader(tc.createGroupRepoRc)
				}

			} else {
				w.WriteHeader(tc.getRepoRc)
				if _, err := w.Write([]byte(tc.getRepoJson)); err != nil {
					fmt.Printf("Error while writing RepoJson, %s", err)
				}
			}

		}))

		defer server.Close()

		dist := rpmDist{*NewBaseDist("rpm", testConfig("nexus", server.URL), "basedirectory")}
		testNexus := nexus.Server{Url: server.URL}
		err := dist.CheckNexusGroupRepository(testNexus, mock_group_name)
		assert.Equal(t, fmt.Sprint(tc.expected), fmt.Sprint(err), "Error mis-match, Test Case: "+key)
	}
}

func TestRpmInstallLocal(t *testing.T) {
	placeholderComp := testComponent("testPurl")
	testCases := map[string]struct {
		config   *configs.DropprConfig
		testCmd  string
		fileName string
		expected Result
	}{
		"good": {
			testConfig("localinstall", ""),
			"echo",
			"file1.rpm",
			Result{true, placeholderComp, ""},
		},
		"config repo error": {
			&configs.DropprConfig{},
			"echo",
			"file1.rpm",
			Result{false, placeholderComp, "Error installing pkg:rpm/name/space/file1.rpm component locally: Unable to locate configuration for Purl type rpm matching repository http://my-repo"},
		},
		"install failure": {
			testConfig("localinstall", ""),
			"ls",
			"file1.rpm",
			Result{false, placeholderComp, "exit status 2: ls: invalid option -- 'y'\nTry 'ls --help' for more information."},
		},
	}

	for key, tc := range testCases {
		for idx := range tc.config.Repos {
			tc.config.Repos[idx].Local_Install.Package_Manager_Command = []string{tc.testCmd}
		}

		dist := rpmDist{*NewBaseDist("rpm", tc.config, "basedirectory")}
		comp := testComponent("pkg:rpm/name/space/file1.rpm")

		result := dist.installFileLocal(comp, dist.buildCommand, tc.fileName)
		assert.Equal(t, tc.expected.Success, result.Success, "Result Status mis-match, Test Case: "+key)
		assert.Equal(t, comp, result.Component, "Component mis-match, Test Case: "+key)
		assert.Equal(t, tc.expected.Message, result.Message, "Message mis-match, Test Case: "+key)
	}

}

func TestRpmGetRepoInfo(t *testing.T) {
	testCases := map[string]struct {
		collectiondir string
		expected      yumRepoInfo
	}{
		"good-nexus-repo": {
			"rpm/https%3A%2F%2Fnexus.global.lmco.com%2Frepository%2Fyum-rocky-proxy/8.6/AppStream/x86_64/os/Packages/g",
			yumRepoInfo{float64(4), "8.6/AppStream/x86_64/os/Packages/g"},
		},
		"good-non-nexus-repo": {
			"rpm/http%3A%2F%2Fatl.mirrors.clouvider.net%2Frocky%2F8%2FBaseOS%2Fx86_64%2Fos/Packages/g",
			yumRepoInfo{0, "Packages/g"},
		},
	}

	for key, tc := range testCases {
		result := getRepoInfo(tc.collectiondir)

		assert.Equal(t, tc.expected.repoDepth, result.repoDepth, "Result depth mis-match, Test Case: "+key)
		assert.Equal(t, tc.expected.targetDir, result.targetDir, "Target directory info mis-match, Test Case: "+key)
	}

}

func TestRpmAddRepoToGroup(t *testing.T) {
	testCases := map[string]struct {
		repoName string
		expected bool
	}{
		"add-to-repo": {
			"some-repo1",
			true,
		},
		"already-exists": {
			"some-repo2",
			false,
		},
	}
	groupRepoMembers = []string{"some-repo2", "another-repo2"}

	for key, tc := range testCases {
		result := addRepoToGroup(tc.repoName)

		assert.Equal(t, tc.expected, result, "Expected and result mis-match, Test Case: "+key)
	}

}

func TestFindRpmFile(t *testing.T) {
	testCases := map[string]struct {
		purlString string
		expected   string
	}{
		"equal":         {"pkg:rpm/simpleTest@1.2.3?arch=x86", "simpleTest-1.2.3.x86.rpm"},
		"diff sep":      {"pkg:rpm/mitre-photon-bindings@0.8.0-20220614152837.el7?arch=x86_64", "mitre-photon-bindings-0.8.0+20220614152837.el7.x86_64.rpm"},
		"diff len":      {"pkg:rpm/nsight-systems-2023.1.2@2023.1.2.43_3237721-0?arch=x86_64", "nsight-systems-2023.1.2-2023.1.2.43_32377213v0-0.x86_64.rpm"},
		"nothing found": {"pkg:rpm/something-we-dont-have@1.2.3", ""},
		"prefix test":   {"pkg:rpm/kubectl@1.24.7-0?arch=x86_64&upstream=kubelet-1.24.7-0.src.rpm", "ee8dcef1201e2babfa8af444be97bad54911eff6c93555936aa3f2a31d1f0de0-kubectl-1.24.7-0.x86_64.rpm"},
	}

	mockFileSys := fstest.MapFS{}

	fileNames := []string{
		"simpleTest-1.2.3.noarch.rpm",
		"simpleTest-1.2.3.x86.rpm",
		"mitre-photon-bindings-0.8.0+20220614152837.el7.x86_64.rpm",
		"nsight-systems-2023.1.2-2023.1.2.43_32377213v0-0.x86_64.rpm",
		"ee8dcef1201e2babfa8af444be97bad54911eff6c93555936aa3f2a31d1f0de0-kubectl-1.24.7-0.x86_64.rpm",
	}

	fileList := []os.DirEntry{}

	for _, name := range fileNames {
		mockFileSys[name] = &fstest.MapFile{}
		fi, _ := mockFileSys.Stat(name)
		de := fs.FileInfoToDirEntry(fi)
		fileList = append(fileList, de)
	}

	for key, tc := range testCases {
		purl := ParsePurl(tc.purlString)
		if tc.expected == "" {
			assert.Panics(t, func() { find_rpm_file(purl, fileList) })
		} else {
			actual := find_rpm_file(purl, fileList)
			assert.Equal(t, tc.expected, actual, "Mis-match, Test Case: "+key)
		}
	}

}

func TestRpmDefaultCommand(t *testing.T) {
	config := testConfig("localinstall", "")
	dist := rpmDist{*NewBaseDist("rpm", config, "basedirectory")}
	comp := testComponent("pkg:apt/name/space/file1")

	actual_command, err := dist.buildCommand("file/name", comp)
	assert.Equal(t, nil, err)
	
	_, err = exec.LookPath("sudo")

	if err == nil {
		assert.Equal(t, []string{"sudo", "dnf",  "install", "-y", "file/name"}, actual_command)
	} else {
		assert.Equal(t, []string{"dnf",  "install", "-y", "file/name"}, actual_command)
	}
}
