/*
 *  File: pypi_dist_test.go
 *  Copyright © 2023 Lockheed Martin <open.source@lmco.com>
 *
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 */

package install

import (
	"bytes"
	"fmt"
	"io"
	"io/fs"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"
	"testing/fstest"

	"github.com/stretchr/testify/assert"

	"gitlab.com/hoppr/droppr/pkg/configs"
	"gitlab.com/hoppr/droppr/pkg/nexus"
)

func TestPypiInstallNexus(t *testing.T) {
	placeholderComp := testComponent("testPurl")
	testCases := map[string]struct {
		fileNames []string
		uploadRc  []int
		openError bool
		expected  Result
	}{
		"good": {
			[]string{"alpha_1.2.3.pom", "beta_4.5.jar", "gamma_6.7.8.jar"},
			[]int{200, 200, 200},
			false,
			Result{true, placeholderComp, ""},
		},
		"dir read error": {
			[]string{"alpha_1.2.3.pom", "ERRbeta_4.5.jar", "gamma_6.7.8.jar"},
			[]int{200, 200, 200},
			false,
			Result{false, placeholderComp, "Mock Directory Read Error"},
		},
		"upload failure": {
			[]string{"alpha_1.2.3.pom", "beta_4.5.jar", "gamma_6.7.8.jar"},
			[]int{200, 200, 400},
			false,
			Result{false, placeholderComp, "Error response from upload HTTP call: 400 Bad Request"},
		},
		"read error": {
			[]string{"alpha_1.2.3.pom", "beta_4.5.jar", "gamma_6.7.8.jar"},
			[]int{200, 200, 200},
			true,
			Result{false, placeholderComp, "Mock Open Error"},
		},
	}

	for key, tc := range testCases {
		serverCallIndex := 0
		server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(tc.uploadRc[serverCallIndex])
			serverCallIndex++
		}))
		defer server.Close()

		dist := pypiDist{*NewBaseDist("pypi", testConfig("nexus", server.URL), "basedirectory")}
		comp := testComponent("pkg:pypi/name/space/TestPypi@1.2.3")

		testNexus := nexus.Server{Url: server.URL}

		mockFileSys := fstest.MapFS{}
		for _, name := range tc.fileNames {
			if strings.HasSuffix(name, "/") {
				mockFileSys[strings.TrimRight(name, "/")] = &fstest.MapFile{Mode: os.ModeDir}
			} else {
				mockFileSys[name] = &fstest.MapFile{}
			}
		}

		mockReadDir := func(name string) ([]os.DirEntry, error) {
			content := []os.DirEntry{}
			for name := range mockFileSys {
				if strings.HasPrefix(name, "ERR") {
					return content, fmt.Errorf("Mock Directory Read Error")
				}
				fi, _ := mockFileSys.Stat(name)
				content = append(content, fs.FileInfoToDirEntry(fi))
			}
			return content, nil
		}

		mockOpen := func(name string) (io.Reader, error) {
			if tc.openError {
				return bytes.NewReader([]byte("")), fmt.Errorf("Mock Open Error")
			}
			return bytes.NewReader([]byte("Data from file " + name)), nil
		}

		result := dist.installNexus(comp, testNexus, "test_pypi_repo", mockReadDir, mockOpen)

		assert.Equal(t, tc.expected.Success, result.Success, "Result Status mis-match, Test Case: "+key)
		assert.Equal(t, comp, result.Component, "Component mis-match, Test Case: "+key)
		assert.Equal(t, tc.expected.Message, result.Message, "Message mis-match, Test Case: "+key)
	}
}

func TestPypiCheckNexusRepository(t *testing.T) {
	testCases := map[string]struct {
		getRepoRc    int
		getRepoJson  string
		createRepoRc int
		expected     error
	}{
		"got repo": {
			200,
			"[{\"name\": \"test-pypi-repo\", \"format\": \"pypi\", \"type\": \"hosted\"}]",
			200,
			nil,
		},
		"invalid repo": {
			200,
			"[{\"name\": \"test-pypi-repo\", \"format\": \"maven2\", \"type\": \"hosted\"}]",
			200,
			fmt.Errorf("Repository test-pypi-repo has format 'maven2', format 'pypi' requested"),
		},
		"create repo": {
			404,
			"[{\"name\": \"good-repo\", \"format\": \"pypi\", \"type\": \"hosted\"}]",
			200,
			nil,
		},
		"get repo failure": {
			401,
			"[{\"name\": \"good-repo\", \"format\": \"pypi\", \"type\": \"hosted\"}]",
			200,
			fmt.Errorf("Error response from HTTP call: 401 Unauthorized"),
		},
		"create repo failure": {
			404,
			"[{\"name\": \"good-repo\", \"format\": \"pypi\", \"type\": \"hosted\"}]",
			401,
			fmt.Errorf("Error response from HTTP call to build repository: 401 Unauthorized"),
		},
	}

	for key, tc := range testCases {
		server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if strings.HasSuffix(fmt.Sprint(r.URL), "/hosted") {
				w.WriteHeader(tc.createRepoRc)
			} else {
				w.WriteHeader(tc.getRepoRc)
				if _, err := w.Write([]byte(tc.getRepoJson)); err != nil {
					fmt.Printf("Error while writing RepoJson, %s", err)
				}
			}
		}))
		defer server.Close()

		dist := pypiDist{*NewBaseDist("pypi", testConfig("nexus", server.URL), "basedirectory")}

		testNexus := nexus.Server{Url: server.URL}

		err := dist.CheckNexusRepository(testNexus, "test-pypi-repo")

		assert.Equal(t, fmt.Sprint(tc.expected), fmt.Sprint(err), "Error mis-match, Test Case: "+key)
	}
}

func TestPypiInstallLocal(t *testing.T) {
	placeholderComp := testComponent("testPurl")
	testCases := map[string]struct {
		config    *configs.DropprConfig
		testCmd   string
		fileNames []string
		expected  Result
	}{
		"good": {
			testConfig("localinstall", ""),
			"echo",
			[]string{"alpha_1.2.3.pom", "beta_4.5.jar", "gamma_6.7.8.jar"},
			Result{true, placeholderComp, ""},
		},
		"config repo error": {
			&configs.DropprConfig{},
			"echo",
			[]string{"alpha_1.2.3.pom", "beta_4.5.jar", "gamma_6.7.8.jar"},
			Result{false, placeholderComp, "Error installing pkg:pypi/name/space/TestPypi@1.2.3 component locally: Unable to locate configuration for Purl type pypi matching repository http://my-repo"},
		},
		"dir read error": {
			testConfig("localinstall", ""),
			"echo",
			[]string{"alpha_1.2.3.pom", "ERRbeta_4.5.jar", "gamma_6.7.8.jar"},
			Result{false, placeholderComp, "Mock Directory Read Error"},
		},
		"install failure": {
			testConfig("localinstall", ""),
			"ls",
			[]string{"alpha_1.2.3.pom", "beta_4.5.jar", "gamma_6.7.8.jar"},
			Result{false, placeholderComp, "exit status 2: ls: unrecognized option '--no-deps'\nTry 'ls --help' for more information."},
		},
	}

	for key, tc := range testCases {
		for idx := range tc.config.Repos {
			tc.config.Repos[idx].Local_Install.Package_Manager_Command = []string{tc.testCmd}
		}

		dist := pypiDist{*NewBaseDist("pypi", tc.config, "basedirectory")}
		comp := testComponent("pkg:pypi/name/space/TestPypi@1.2.3")

		mockFileSys := fstest.MapFS{}
		for _, name := range tc.fileNames {
			if strings.HasSuffix(name, "/") {
				mockFileSys[strings.TrimRight(name, "/")] = &fstest.MapFile{Mode: os.ModeDir}
			} else {
				mockFileSys[name] = &fstest.MapFile{}
			}
		}

		mockReadDir := func(name string) ([]os.DirEntry, error) {
			content := []os.DirEntry{}
			for name := range mockFileSys {
				if strings.HasPrefix(name, "ERR") {
					return content, fmt.Errorf("Mock Directory Read Error")
				}
				fi, _ := mockFileSys.Stat(name)
				content = append(content, fs.FileInfoToDirEntry(fi))
			}
			return content, nil
		}

		result := dist.installDirLocal(comp, dist.buildCommand, mockReadDir)

		assert.Equal(t, tc.expected.Success, result.Success, "Result Status mis-match, Test Case: "+key)
		assert.Equal(t, comp, result.Component, "Component mis-match, Test Case: "+key)
		assert.Equal(t, tc.expected.Message, result.Message, "Message mis-match, Test Case: "+key)
	}
}

func TestPypiDefaultCommand(t *testing.T) {
	config := testConfig("localinstall", "")
	dist := pypiDist{*NewBaseDist("pypi", config, "basedirectory")}
	comp := testComponent("pkg:apt/name/space/file1")

	actual_command, err := dist.buildCommand("file/name", comp)
	assert.Equal(t, nil, err)
	assert.Equal(t, []string{"pip",  "install", "--no-deps", "--no-input", "-v", "file/name"}, actual_command)
}
