/*
 *  File: distribute.go
 *  Copyright © 2023 Lockheed Martin <open.source@lmco.com>
 *
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 */

package install

import (
	"fmt"
	"net/url"
	"os"
	"os/exec"
	"regexp"
	"strings"
	"unicode"

	cdx "github.com/CycloneDX/cyclonedx-go"

	"gitlab.com/hoppr/droppr/pkg/configs"
	"gitlab.com/hoppr/droppr/pkg/logging"
	"gitlab.com/hoppr/droppr/pkg/nexus"
	"gitlab.com/hoppr/droppr/pkg/utils"
)

type distributor interface {
	InstallLocal(comp cdx.Component) Result
	InstallNexus(comp cdx.Component) Result
	InstallFilesys(comp cdx.Component) Result
	GetRepoConfig(comp cdx.Component) (*configs.PackageType, error)
	Log() *logging.MemoryLogger
}

const collectionDirectory = "hoppr:collection:directory"
const collectionPlugin = "hoppr:collection:plugin"
const collectionRepository = "hoppr:collection:repository"
const collectionTimetag = "hoppr:collection:timetag"

type baseDist struct {
	purlType string
	config   *configs.DropprConfig
	baseDir  string
	log      *logging.MemoryLogger
}

func NewBaseDist(purlType string, config *configs.DropprConfig, baseDir string) *baseDist {
	base := new(baseDist)
	base.purlType = purlType
	base.config = config
	base.baseDir = baseDir

	base.log = logging.NewMemoryLogger(purlType)

	return base
}

func (self *baseDist) Log() *logging.MemoryLogger {
	return self.log
}

func (self *baseDist) InstallLocal(comp cdx.Component) Result {
	return Result{false, comp, fmt.Sprint("InstallLocal function not defined for ", self.purlType)}
}

func (self *baseDist) InstallNexus(comp cdx.Component) Result {
	return Result{false, comp, fmt.Sprint("InstallNexus function not defined for ", self.purlType)}
}

func (self *baseDist) InstallFilesys(comp cdx.Component) Result {
	return self.installFilesys(comp, utils.CopyDir)
}

func (self *baseDist) installFilesys(comp cdx.Component, copyFunc func(string, string) error) Result {
	collectionData := getCollectionData(comp)

	repoConfig, err := self.GetRepoConfig(comp)
	if err != nil {
		msg := fmt.Sprintf("Error installing %s component to local file system: %s", comp.PackageURL, err)
		self.Log().Error(msg)
		return Result{false, comp, msg}
	}

	if repoConfig.File_System.Target_Directory == "" {
		msg := "Filesystem install target directory not specified"
		self.Log().Error(msg)
		return Result{false, comp, msg}
	}

	srcDir := self.baseDir + string(os.PathSeparator) + collectionData[collectionDirectory]
	tgtDir := repoConfig.File_System.Target_Directory + string(os.PathSeparator) + collectionData[collectionDirectory]
	err = copyFunc(srcDir, tgtDir)
	if err == nil {
		return Result{true, comp, ""}
	}

	msg := fmt.Sprintf("Unable to copy from %s to %s, %s.", srcDir, tgtDir, err)
	self.Log().Error(msg)
	return Result{false, comp, msg}
}

func getCollectionData(comp cdx.Component) map[string]string {
	collectionData := map[string]string{}

	for _, prop := range *comp.Properties {
		if strings.HasPrefix(prop.Name, "hoppr:collection:") {
			collectionData[prop.Name] = prop.Value
		}
	}

	return collectionData
}

func (self *baseDist) GetRepoConfig(comp cdx.Component) (*configs.PackageType, error) {
	// Locate the first PackageType that matches the Purl type and the regex

	// Pulling the repo from the directory because the Nexus Search Collector in Hoppr may only list the Nexus URL as the repository
	dir := strings.Split(getCollectionData(comp)[collectionDirectory], "/")
	repo, _ := url.QueryUnescape(dir[1])

	for _, p := range self.config.Repos {
		if p.Purl_Type == self.purlType {
			re, err := regexp.Compile(p.Regex_Match)
			if err != nil {
				return nil, fmt.Errorf("Problem with regex: %s", err.Error())
			}

			matches := re.FindStringSubmatch(repo)

			if len(matches) > 0 {
				// Get a deep copy of the config package type before we
				// modify the target directories/repo names for regexp back refs
				repoConfig := p.Clone()

				if repoConfig.File_System != nil {
					repoConfig.File_System.Target_Directory = subBackRefs(repoConfig.File_System.Target_Directory, matches)
				}
				if repoConfig.Local_Install != nil {
					repoConfig.Local_Install.Target_Directory = subBackRefs(repoConfig.Local_Install.Target_Directory, matches)
				}
				if repoConfig.Nexus != nil {
					repoConfig.Nexus.Repo_Name = subBackRefs(repoConfig.Nexus.Repo_Name, matches)
				}
				return &repoConfig, nil
			}
		}
	}

	return nil, fmt.Errorf("Unable to locate configuration for Purl type %s matching repository %s", self.purlType, repo)
}

func subBackRefs(input string, backrefs []string) string {
	// Perform backreference substitutions on a string.
	//
	// Doing it character-by-character because of edge cases (including escaping $
	// with $$) that would make regexp solutions at least as complex.

	if len(backrefs) == 0 || !strings.Contains(input, "$") {
		return input
	}

	chars_out := []rune{}
	chars_in := []rune(input)
	for i := 0; i < len(chars_in); i++ {
		c := chars_in[i]

		// Just copy the character if it's not a $
		//      or it's the last character of the string,

		if c != '$' || i == len(chars_in)-1 {
			chars_out = append(chars_out, c)
			continue
		}

		// If it's followed by a $, skip ahead one

		if chars_in[i+1] == '$' {
			chars_out = append(chars_out, c)
			i++
			continue
		}

		// if it's not followed by a digit, just copy it

		if !unicode.IsDigit(chars_in[i+1]) {
			chars_out = append(chars_out, c)
			continue
		}

		match_index := int(chars_in[i+1] - '0')

		// No backref, no substitution

		if match_index >= len(backrefs) {
			chars_out = append(chars_out, c)
			continue
		}

		chars_out = append(chars_out, []rune(backrefs[match_index])...)
		i++
	}

	return string(chars_out)
}

func (self *baseDist) ValidateNexusRepository(
	repo *nexus.Repository,
	format string,
	additionalParams map[string]interface{},
) error {
	// Distributors (e.g. Docker) may override this method if more involved checks are needed

	if repo.Format != format {
		err := fmt.Errorf("Repository %s has format '%s', format '%s' requested", repo.Name, repo.Format, format)
		return err
	}

	if repo.Type != "hosted" {
		err := fmt.Errorf("Repository %s has type '%s', only hosted repositories are supported", repo.Name, repo.Type)
		return err
	}

	return nil
}

func (self *baseDist) installDirLocal(
	comp cdx.Component,
	buildCmdFunc func(fn string, comp cdx.Component) ([]string, error),
	readDirFunc func(filename string) ([]os.DirEntry, error),
) Result {

	dir := self.baseDir + string(os.PathSeparator) + getCollectionData(comp)[collectionDirectory]

	files, err := readDirFunc(dir)
	if err != nil {
		return Result{false, comp, err.Error()}
	}

	for _, f := range files {
		fqfn := dir + string(os.PathSeparator) + f.Name()

		result := self.installFileLocal(comp, buildCmdFunc, fqfn)
		if !result.Success {
			return result
		}
	}

	return Result{true, comp, ""}
}

func (self *baseDist) installFileLocal(
	comp cdx.Component,
	buildCmdFunc func(fn string, comp cdx.Component) ([]string, error),
	fqfn string,
) Result {

	command, err := buildCmdFunc(fqfn, comp)
	if err != nil {
		self.Log().Errorf(err.Error())
		return Result{false, comp, err.Error()}
	}

	// If command comes back empty, we skip processing this file
	if len(command) == 0 {
		return Result{true, comp, ""}
	}

	output, err := runCommand(command)
	self.Log().Println("Command output:\n     " + strings.ReplaceAll(output, "\n", "\n     "))
	if err != nil {
		return Result{false, comp, err.Error()}
	}

	return Result{true, comp, ""}
}

func runCommand(command []string) (string, error) {
	return runCommandIn(command, "")
}

func runCommandIn(command []string, dir string) (string, error) {
	if len(command) == 0 {
		return "", fmt.Errorf("No command specified")
	}

	cmd := exec.Command(command[0], command[1:]...)
	cmd.Dir = dir

	output, err := cmd.Output()
	if exerr, ok := err.(*exec.ExitError); ok {
		err = fmt.Errorf(err.Error() + ": " + strings.TrimRight(string(exerr.Stderr), "\n"))
	}

	return string(output), err
}
